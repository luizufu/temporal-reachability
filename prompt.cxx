#include "prompt.hxx"

#include <iostream>
#include <iterator>
#include <sstream>
#include <string_view>

constexpr size_t KEY_UP = 72;
constexpr size_t KEY_DOWN = 80;

static void clear_screen()
{
#ifdef __linux__
    std::printf("\033c"); // NOLINT
#elif _WIN32
    system("cls");
#endif
}

static auto pad_left(std::string_view str, const size_t length, const char chr)
    -> std::string
{
    std::string ret(str);
    ret.insert(ret.begin(), (length + 2) - str.size(), chr);
    return ret;
}

static auto pad_right(std::string_view str, const size_t length, const char chr)
    -> std::string
{
    std::string ret(str);
    ret.insert(ret.end(), (length + 2) - str.size(), chr);
    return ret;
}

static void handle_exit_code(exit_code code)
{
    if(code == exit_code::invalid_arguments)
    {
        std::cout << "Invalid Arguments, type \"help\" for details.\n";
    }
    else if(code == exit_code::error)
    {
        std::cout << "Found some error during interval computation.\n";
    }
}

static auto parse_input() -> std::vector<std::string>
{
    std::string str;
    std::getline(std::cin, str);
    std::istringstream iss(str);

    return {std::istream_iterator<std::string> {iss},
            std::istream_iterator<std::string> {}};
}

prompt::prompt(std::string delim, std::string greeting, std::string farewell)
    : _delimiter(std::move(delim))
    , _greeting(std::move(greeting))
    , _farewell(std::move(farewell))
    , _col1_width(0)
    , _col2_width(0)
{
}

void prompt::print_help()
{
    for(size_t i = 0; i < _commands.size(); ++i)
    {
        std::cout << pad_left(_commands[i].name, _col1_width, ' ') << " "
                  << pad_right(_commands[i].help.arguments, _col2_width, ' ') << " "
                  << _commands[i].help.description << "\n";

        if(_lines[i])
        {
            std::cout << '\n';
        }
    }

    std::cout << pad_left("help", _col1_width, ' ') << " "
              << pad_right("", _col2_width, ' ') << " "
              << "This command\n";
    std::cout << pad_left("quit", _col1_width, ' ') << " "
              << pad_right("", _col2_width, ' ') << " "
              << "Terminates the execution\n";
}

void prompt::start()
{
    clear_screen();

    std::cout << _greeting << '\n';

    while(true)
    {
        std::cout << '\n' << _delimiter;

        if(auto args = parse_input(); !args.empty())
        {
            if(args[0] == "quit")
            {
                break;
            }

            if(args[0] == "help")
            {
                print_help();
                continue;
            }

            if(auto it = _mapping.find(args[0]); it != _mapping.end())
            {
                exit_code code = _commands[it->second].cmd(args);
                handle_exit_code(code);
            }
            else
            {
                std::cout << "Command not found.\n";
            }
        }
    }

    std::cout << _farewell << std::endl;
}

void prompt::add_command(std::string name, cmd_callable cmd, command_help help)
{
    _col1_width = std::max(_col1_width, name.size());
    _col2_width = std::max(_col2_width, help.arguments.size());
    _mapping[name] = _commands.size();
    _commands.push_back({std::move(name), std::move(cmd), help});
    _lines.push_back(false);
}

void prompt::add_line()
{
    _lines.back() = true;
}
