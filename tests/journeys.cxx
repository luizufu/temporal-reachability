#include "tci/baseline.hxx"
#include "tci/transitive-closure.hxx"
#include "tests/random/random-generator.hxx"
#include "tests/random/random-graph.hxx"

#include <cmath>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <random>
#include <vector>

struct query
{
    tci::vertex u, v;
    tci::query_interval interval;
};

auto dominates(const tci::journey& lhs, const tci::journey& rhs) -> bool;
void sort_by_arrival_asc(std::vector<tci::journey>* js);
void remove_by_discarding_rule_asc(std::vector<tci::journey>* js);

template<typename Container>
void shuffle_vector(Container& container);

auto generate_interval(uint32_t tau) -> tci::query_interval;
auto generate_queries(uint32_t amount, uint32_t n, uint32_t tau) -> std::vector<query>;

template<typename Index1, typename Index2>
auto test_queries(Index1& index1, Index2& index2, uint32_t amount, uint32_t n,
                  uint32_t tau) -> bool;

auto test_journeys(uint32_t n_queries, uint32_t n, uint32_t d, uint32_t tau) -> bool;

auto main() -> int
{
    const size_t n_start = 16;
    const size_t n_end = 32;
    const size_t n_width = 3;
    const size_t tau_start = 64;
    const size_t tau_end = 128;
    const size_t tau_width = 3;
    const size_t n_queries = 5;

    for(size_t n = n_start; n <= n_end; n *= 2)
    {
        for(size_t tau = tau_start; tau <= tau_end; tau *= 2)
        {
            uint32_t d = std::sqrt(n);
            std::cout << "testing journeys(" << std::setw(n_width) << n << ", "
                      << std::setw(n_width) << d << ", " << std::setw(tau_width) << tau
                      << ")" << std::flush;

            if(!test_journeys(n_queries, n, d, tau))
            {
                std::cout << "\tFAILED" << std::endl;
                return 1;
            }

            std::cout << "\tOK" << std::endl;
        }
    }

    return 0;
}

auto dominates(const tci::journey& lhs, const tci::journey& rhs) -> bool
{
    return lhs.departure() <= rhs.departure() && rhs.arrival() <= lhs.arrival();
}

void sort_by_arrival_asc(std::vector<tci::journey>* js)
{
    std::sort(js->begin(), js->end(),
              [](const tci::journey& lhs, const tci::journey& rhs) {
                  return lhs.arrival() < rhs.arrival()
                         || (!(rhs.arrival() < lhs.arrival())
                             && lhs.departure() > rhs.departure());
              });
}

void remove_by_discarding_rule_asc(std::vector<tci::journey>* js)
{
    std::vector<tci::journey> tmp;

    for(size_t i = 0; i < js->size(); ++i)
    {
        if(std::none_of(js->begin(), js->begin() + static_cast<ptrdiff_t>(i),
                        [&](const tci::journey& j) {
                            return dominates(js->at(i), j);
                        }))
        {
            tmp.push_back(js->at(i));
        }
    }

    *js = tmp;
}

template<typename Container>
void shuffle_vector(Container& container)
{
    std::vector<uint32_t> indexes(container.size());
    std::iota(indexes.begin(), indexes.end(), 0);
    std::shuffle(indexes.begin(), indexes.end(), generator());

    Container shuffled;
    std::transform(indexes.begin(), indexes.end(), std::back_inserter(shuffled),
                   [&container](uint32_t i) {
                       return container[i];
                   });

    container = shuffled;
}

auto generate_interval(uint32_t tau) -> tci::query_interval
{
    std::uniform_int_distribution<uint32_t> tdist(0, tau - 1);

    tci::query_interval interval = {tdist(generator()), tdist(generator())};

    if(interval.t2 < interval.t1)
    {
        std::swap(interval.t1, interval.t2);
    }
    else if(interval.t1 == interval.t2)
    {
        interval.t2 += 1;
    }

    return interval;
}

auto generate_queries(uint32_t amount, uint32_t n, uint32_t tau) -> std::vector<query>
{
    std::uniform_int_distribution<uint32_t> vdist(0, n - 1);
    std::vector<query> queries;

    for(uint32_t i = 0; i < amount; ++i)
    {
        query q = {vdist(generator()), vdist(generator()), generate_interval(tau)};

        queries.push_back(q);
    }

    return queries;
}

template<typename Index1, typename Index2>
auto test_queries(Index1& index1, Index2& index2, uint32_t amount, uint32_t n,
                  uint32_t tau) -> bool
{
    auto queries = generate_queries(amount, n, tau);

    return std::all_of(queries.begin(), queries.end(), [&](const query& q) {
        /* if(q.u == 6 && q.v == 0) */
        /* { */
        /*     std::cout << "debugging" << std::endl; */
        /* } */
        auto js1 = index1.journeys(q.u, q.v, q.interval);
        auto js2 = index2.journeys(q.u, q.v, q.interval);

        /* std::cout << js1.size() << ", " << js2.size() << std::endl; */

        /* sort_by_arrival_asc(&js1); */
        /* sort_by_arrival_asc(&js2); */
        /* remove_by_discarding_rule_asc(&js1); */

        if(js1.size() != js2.size())
        {
            return false;
        }

        for(size_t i = 0; i < js1.size(); ++i)
        {
            if(js1[i] != js2[i])
            {
                /* std::cout << i << ", " << q.u << ", " << q.v << ", [" << js1[i] <<
                 * "], [" << js2[i] << "]" << std::endl; */
                return false;
            }
        }

        return true;
    });
}

auto test_journeys(uint32_t n_queries, uint32_t n, uint32_t d, uint32_t tau) -> bool
{
    auto contacts = make_temporal_price(n, d, tau);
    shuffle_vector(contacts);

    tci::transitive_closure t1(n, tau, 1);
    tci::baseline t2(tau, 1);

    for(const tci::contact& c: contacts)
    {
        t1.insert_contact(c.e.u, c.e.v, c.t);
        t2.insert_contact(c.e.u, c.e.v, c.t);
    }

    return test_queries(t1, t2, n_queries, n, tau);
}
