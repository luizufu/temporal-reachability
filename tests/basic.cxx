#include "tci/detail/jtree.hxx"

#include <iostream>

void print(const tci::detail::jtree& tree);

auto main() -> int
{
    tci::detail::jtree tree;
    tree.insert({0, 5,  10});
    tree.insert({0, 9,  15});
    tree.insert({0, 10, 19});
    tree.insert({0, 20, 50});

    std::vector<tci::journey_schedule> q;
    q.push_back({0, 4, 9});
    /* q.push_back(tci::journey_schedule(0, 5, 10)); */
    q.push_back({0, 6, 11});
    /* q.push_back(tci::journey_schedule(0, 11, 16)); */

    print(tree);
    tree.insert_by_increasing_departure(q.begin(), q.end());
    print(tree);

    return 0;
}

void print(const tci::detail::jtree& tree)
{
    for(auto j: tree)
    {
        std::cout << j << ", ";
    }
    std::cout << std::endl;
}
