#include "tci/transitive-closure.hxx"

#include "tci/baseline.hxx"
#include "tests/random/random-generator.hxx"
#include "tests/random/random-graph.hxx"

#include <cmath>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <random>
#include <vector>

struct query
{
    tci::vertex u, v;
    tci::query_interval interval;
};

template<typename Container>
void shuffle_vector(Container& container);

auto generate_interval(uint32_t tau) -> tci::query_interval;
auto generate_queries(uint32_t amount, uint32_t n, uint32_t tau) -> std::vector<query>;

template<typename Index1, typename Index2>
auto test_queries(Index1& index1, Index2& index2, uint32_t amount, uint32_t n,
                  uint32_t tau) -> bool;

auto test_timestamp_insertion(uint32_t n_queries, uint32_t n, uint32_t d, uint32_t tau)
    -> bool;
auto test_interval_insertion(uint32_t n_queries, uint32_t n, uint32_t d, uint32_t tau)
    -> bool;

auto main() -> int
{
    const size_t n_start = 16;
    const size_t n_end = 32;
    const size_t n_width = 3;
    const size_t tau_start = 64;
    const size_t tau_end = 128;
    const size_t tau_width = 3;
    const size_t n_queries = 50;

    /* for(size_t n = n_start; n <= n_end; n *= 2) */
    /* { */
    /*     for(size_t tau = tau_start; tau <= tau_end; tau *= 2) */
    /*     { */
    /*         uint32_t d = std::sqrt(n); */
    /*         std::cout << "testing timestamp_insertion(" << std::setw(n_width) << n */
    /*                   << ", " << std::setw(n_width) << d << ", " << std::setw(tau_width) */
    /*                   << tau << ")" << std::flush; */

    /*         if(!test_timestamp_insertion(n_queries, n, d, tau)) */
    /*         { */
    /*             std::cout << "\tFAILED" << std::endl; */
    /*             return 1; */
    /*         } */

    /*         std::cout << "\tOK" << std::endl; */
    /*     } */
    /* } */

    for(size_t n = n_start; n <= n_end; n *= 2)
    {
        for(size_t tau = tau_start; tau <= tau_end; tau *= 2)
        {
            uint32_t d = std::sqrt(n);
            std::cout << "testing interval_insertion(" << std::setw(n_width) << n
                      << ", " << std::setw(n_width) << d << ", " << std::setw(tau_width)
                      << tau << ")" << std::flush;

            if(!test_interval_insertion(n_queries, n, d, tau))
            {
                std::cout << "\tFAILED" << std::endl;
                return 1;
            }

            std::cout << "\tOK" << std::endl;
        }
    }

    return 0;
}

template<typename Container>
void shuffle_vector(Container& container)
{
    std::vector<uint32_t> indexes(container.size());
    std::iota(indexes.begin(), indexes.end(), 0);
    std::shuffle(indexes.begin(), indexes.end(), generator());

    Container shuffled;
    std::transform(indexes.begin(), indexes.end(), std::back_inserter(shuffled),
                   [&container](uint32_t i) {
                       return container[i];
                   });

    container = shuffled;
}

auto generate_interval(uint32_t tau) -> tci::query_interval
{
    std::uniform_int_distribution<uint32_t> tdist(0, tau - 1);

    tci::query_interval interval = {tdist(generator()), tdist(generator())};

    if(interval.t2 < interval.t1)
    {
        std::swap(interval.t1, interval.t2);
    }
    else if(interval.t1 == interval.t2)
    {
        interval.t2 += 1;
    }

    return interval;
}

auto generate_queries(uint32_t amount, uint32_t n, uint32_t tau) -> std::vector<query>
{
    std::uniform_int_distribution<uint32_t> vdist(0, n - 1);
    std::vector<query> queries;

    for(uint32_t i = 0; i < amount; ++i)
    {
        query q = {vdist(generator()), vdist(generator()), generate_interval(tau)};

        queries.push_back(q);
    }

    return queries;
}

template<typename Index1, typename Index2>
auto test_queries(Index1& index1, Index2& index2, uint32_t amount, uint32_t n,
                  uint32_t tau) -> bool
{
    auto queries = generate_queries(amount, n, tau);

    return std::all_of(queries.begin(), queries.end(), [&](const query& q) {
        bool res1 = index1.is_connected(q.u, q.v, q.interval);
        bool res2 = index2.is_connected(q.u, q.v, q.interval);

        return res1 == res2;
    });
}

auto test_timestamp_insertion(uint32_t n_queries, uint32_t n, uint32_t d, uint32_t tau)
    -> bool
{
    auto contacts = make_temporal_price(n, d, tau);
    shuffle_vector(contacts);

    tci::baseline t1(tau, 1);
    tci::transitive_closure t2(n, tau, 1);

    return std::all_of(contacts.begin(), contacts.end(), [&](const tci::contact& c) {
        t1.insert_contact(c.e.u, c.e.v, c.t);
        t2.insert_contact(c.e.u, c.e.v, c.t);

        return test_queries(t1, t2, n_queries, n, tau);
    });
}

auto test_interval_insertion(uint32_t n_queries, uint32_t n, uint32_t d, uint32_t tau)
    -> bool
{
    auto edges = make_price(n, d);
    shuffle_vector(edges);

    tci::baseline t1(tau, 1);
    tci::transitive_closure t2(n, tau, 1);

    return std::all_of(edges.begin(), edges.end(), [&](const tci::edge& e) {
        tci::query_interval interval = generate_interval(tau);
        t1.insert_contacts(e.u, e.v, interval);
        t2.insert_contacts(e.u, e.v, interval);

        return test_queries(t1, t2, n_queries, n, tau);
    });
}
