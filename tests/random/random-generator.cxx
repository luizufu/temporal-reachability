#include "tests/random/random-generator.hxx"

const uint32_t YEAR = 'c' + 'o' + 'r' + 'o' + 'n' + 'a' + 2000;

auto generator() -> std::mt19937&
{
    /* static std::random_device r; */
    /* static std::seed_seq seed{r(), r(), r(), r(), r(), r(), r(), r()}; */
    static std::seed_seq seed {YEAR};
    static std::mt19937 gen(seed);

    return gen;
}

auto next_probability() -> float
{
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(generator());
}
