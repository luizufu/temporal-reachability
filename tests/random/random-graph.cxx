#include "tests/random/random-graph.hxx"

#include "tests/random/random-generator.hxx"

#include <unordered_set>

auto make_price(uint32_t n, uint32_t c, uint32_t a) -> std::vector<tci::edge>
{
    if(c == 0)
    {
        return {};
    }

    std::vector<uint32_t> in_degrees(n * c);
    float threshold = c / static_cast<float>(c + a);

    uint32_t size = 0;
    for(; size < c; ++size)
    {
        in_degrees[size] = size;
    }

    for(uint32_t u = c; u < n; ++u)
    {
        std::unordered_set<tci::edge> edges;

        while(edges.size() < c)
        {
            uint32_t v = next_probability() < threshold
                             ? in_degrees[next_probability() * size]
                             : next_probability() * static_cast<float>(u - 1);

            if(edges.insert({u, v}).second)
            {
                in_degrees[size++] = v;
            }
        }
    }

    std::vector<tci::edge> edges;

    for(uint32_t i = 0; i < in_degrees.size(); ++i)
    {
        uint32_t u = i / c;
        uint32_t v = in_degrees[i];
        edges.push_back({u, v});
    }

    return edges;
}

auto make_temporal_price(uint32_t n, uint32_t c, uint32_t tau, float prob, uint32_t a)
    -> std::vector<tci::contact>
{
    if(c == 0)
    {
        return {};
    }

    std::vector<uint32_t> in_degrees(n * c);
    float threshold = c / static_cast<float>(c + a);

    uint32_t size = 0;
    for(; size < c; ++size)
    {
        in_degrees[size] = size;
    }

    for(uint32_t u = c; u < n; ++u)
    {
        std::unordered_set<tci::edge> edges;

        while(edges.size() < c)
        {
            uint32_t v = next_probability() < threshold
                             ? in_degrees[next_probability() * size]
                             : next_probability() * static_cast<float>(u - 1);

            if(edges.insert({u, v}).second)
            {
                in_degrees[size++] = v;
            }
        }
    }

    std::vector<tci::contact> contacts;

    for(uint32_t i = 0; i < in_degrees.size(); ++i)
    {
        uint32_t u = i / c;
        uint32_t v = in_degrees[i];

        for(uint32_t t = 0; t < tau; ++t)
        {
            if(next_probability() < prob)
            {
                contacts.push_back({{u, v}, t});
            }
        }
    }

    return contacts;
}
