#pragma once

#include "tci/types.hxx"

#include <vector>

const float DEFAULT_ACTIVATION_PROB = 0.3F;

auto make_price(uint32_t n, uint32_t c, uint32_t a = 1) -> std::vector<tci::edge>;

// n = n, m = m * c, mt == edges.size()
auto make_temporal_price(uint32_t n, uint32_t c, uint32_t tau,
                         float prob = DEFAULT_ACTIVATION_PROB, uint32_t a = 1)
    -> std::vector<tci::contact>;
