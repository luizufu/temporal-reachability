#pragma once

#include <random>

auto generator() -> std::mt19937&;
auto next_probability() -> float;
