#include "tci/detail/jtree.hxx"

#include "tci/utilities/range.hxx"
#include "tests/random/random-generator.hxx"

#include <cmath>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <random>
#include <vector>

using journey_vector = std::vector<tci::journey_schedule>;
using mult_test_set = std::vector<journey_vector>;

void sort_by_arrival_asc(journey_vector* js);
void sort_by_arrival_desc(journey_vector* js);

template<typename Container>
void shuffle_vector(Container* container);

auto dominates(tci::journey_schedule lhs, tci::journey_schedule rhs) -> bool;
void remove_by_discarding_rule_asc(journey_vector* js);
void remove_by_discarding_rule_desc(journey_vector* js);

auto get_sorted_incomparables(journey_vector js) -> journey_vector;
auto random_only_incomparable_journeys(uint32_t amount, uint32_t tau, uint32_t delta)
    -> journey_vector;
auto random_not_only_incomparable_journeys(uint32_t amount, uint32_t tau,
                                           uint32_t delta) -> journey_vector;

auto make_mult_test_sets(const std::vector<tci::journey_schedule>& journeys)
    -> std::vector<mult_test_set>;
void preprocess_journey_vectors_inside(std::vector<mult_test_set>* test_sets);

auto test_single_test_set_sequential(const journey_vector& incomparables,
                                     const journey_vector& test) -> bool;
auto test_single_test_set_shuffled(const journey_vector& incomparables,
                                   journey_vector* test) -> bool;
auto test_mult_test_set_sequential(const journey_vector& incomparables,
                                   const mult_test_set& test) -> bool;
auto test_mult_test_set_shuffled(const journey_vector& incomparables,
                                 mult_test_set* test) -> bool;

auto test_single_args_only_incomparable(uint32_t amount, uint32_t tau, uint32_t delta)
    -> bool;
auto test_single_args_not_only_incomparable(uint32_t amount, uint32_t tau,
                                            uint32_t delta) -> bool;
auto test_mult_args_only_incomparable(uint32_t amount, uint32_t tau, uint32_t delta)
    -> bool;
auto test_mult_args_not_only_incomparable(uint32_t amount, uint32_t tau, uint32_t delta)
    -> bool;

auto main() -> int
{
    const size_t n_tests = 10;
    const size_t amount_start = 4;
    const size_t amount_width = 4;
    const size_t tau_start = 7;
    const size_t tau_width = 5;

    for(size_t i = 0; i < n_tests; ++i)
    {
        auto amount = static_cast<uint32_t>(1UL << (amount_start + i));
        auto tau = static_cast<uint32_t>(1UL << (tau_start + i));

        std::cout << "testing sing_args_only_incomparable(" << std::setw(amount_width)
                  << amount << ", " << std::setw(tau_width) << tau << ", 1)"
                  << std::flush;

        if(!test_single_args_only_incomparable(amount, tau, 1))
        {
            std::cout << "\tFAILED" << std::endl;
            return 1;
        }

        std::cout << "\tOK" << std::endl;
    }

    for(size_t i = 0; i < n_tests; ++i)
    {
        auto amount = static_cast<uint32_t>(1UL << (amount_start + i));
        auto tau = static_cast<uint32_t>(1UL << (tau_start + i));

        std::cout << "testing sing_args_not_only_incomparable("
                  << std::setw(amount_width) << amount << ", " << std::setw(tau_width)
                  << tau << ", 1)" << std::flush;

        if(!test_single_args_not_only_incomparable(amount, tau, 1))
        {
            std::cout << "\nFAILED" << std::endl;
            return 1;
        }

        std::cout << "\tOK" << std::endl;
    }

    for(size_t i = 0; i < n_tests; ++i)
    {
        auto amount = static_cast<uint32_t>(1UL << (amount_start + i));
        auto tau = static_cast<uint32_t>(1UL << (tau_start + i));

        std::cout << "testing mult_args_only_incomparable(" << std::setw(amount_width)
                  << amount << ", " << std::setw(tau_width) << tau << ", 1)"
                  << std::flush;

        if(!test_mult_args_only_incomparable(amount, tau, 1))
        {
            std::cout << "\tFAILED" << std::endl;
            return 1;
        }

        std::cout << "\tOK" << std::endl;
    }

    for(size_t i = 0; i < n_tests; ++i)
    {
        auto amount = static_cast<uint32_t>(1UL << (amount_start + i));
        auto tau = static_cast<uint32_t>(1UL << (tau_start + i));

        std::cout << "testing mult_args_not_only_incomparable("
                  << std::setw(amount_width) << amount << ", " << std::setw(tau_width)
                  << tau << ", 1)" << std::flush;

        if(!test_mult_args_not_only_incomparable(amount, tau, 1))
        {
            std::cout << "\tFAILED" << std::endl;
            return 1;
        }

        std::cout << "\tOK" << std::endl;
    }

    return 0;
}

void sort_by_arrival_asc(journey_vector* js)
{
    std::sort(js->begin(), js->end(),
              [](tci::journey_schedule lhs, tci::journey_schedule rhs) {
                  return lhs.t_plus < rhs.t_plus
                         || (!(rhs.t_plus < lhs.t_plus)
                             && lhs.t_minus > rhs.t_minus);
              });
}

void sort_by_arrival_desc(journey_vector* js)
{
    std::sort(js->begin(), js->end(),
              [](tci::journey_schedule lhs, tci::journey_schedule rhs) {
                  return lhs.t_plus > rhs.t_plus
                         || (!(rhs.t_plus > lhs.t_plus)
                             && lhs.t_minus < rhs.t_minus);
              });
}

template<typename Container>
void shuffle_vector(Container* container)
{
    std::vector<uint32_t> indexes(container->size());
    std::iota(indexes.begin(), indexes.end(), 0);
    std::shuffle(indexes.begin(), indexes.end(), generator());

    Container shuffled;
    std::transform(indexes.begin(), indexes.end(), std::back_inserter(shuffled),
                   [&container](uint32_t i) {
                       return container->at(i);
                   });

    *container = shuffled;
}

auto dominates(tci::journey_schedule lhs, tci::journey_schedule rhs) -> bool
{
    return lhs.t_minus <= rhs.t_minus && rhs.t_plus <= lhs.t_plus;
}

void remove_by_discarding_rule_asc(journey_vector* js)
{
    journey_vector tmp;
    for(size_t i = 0; i < js->size(); ++i)
    {
        if(std::none_of(js->begin(), js->begin() + static_cast<ptrdiff_t>(i),
                        [&](tci::journey_schedule j) {
                            return dominates(js->at(i), j);
                        }))
        {
            tmp.push_back(js->at(i));
        }
    }

    *js = tmp;
}

void remove_by_discarding_rule_desc(journey_vector* js)
{
    journey_vector tmp;
    for(size_t i = 0; i < js->size(); ++i)
    {
        if(std::none_of(js->begin() + static_cast<ptrdiff_t>(i + 1), js->end(),
                        [&](tci::journey_schedule j) {
                            return dominates(js->at(i), j);
                        }))
        {
            tmp.push_back(js->at(i));
        }
    }

    *js = tmp;
}

auto get_sorted_incomparables(journey_vector js) -> journey_vector
{
    sort_by_arrival_asc(&js);
    remove_by_discarding_rule_asc(&js);

    return js;
}

auto random_only_incomparable_journeys(uint32_t amount, uint32_t tau, uint32_t delta)
    -> journey_vector
{
    std::vector<uint32_t> departures(tau);
    std::vector<uint32_t> arrivals(tau);

    std::iota(departures.begin(), departures.end(), 0);
    std::iota(arrivals.begin(), arrivals.end(), 0);

    std::shuffle(departures.begin(), departures.end(), generator());
    std::shuffle(arrivals.begin(), arrivals.end(), generator());

    if(tau > amount)
    {
        departures.resize(amount);
        arrivals.resize(amount);
    }

    std::sort(departures.begin(), departures.end(), std::greater<>());
    std::sort(arrivals.begin(), arrivals.end(), std::greater<>());

    journey_vector journeys;

    for(uint32_t i = 0; i < amount; ++i)
    {
        if(!(departures[i] < arrivals[i]))
        {
            std::swap(departures[i], arrivals[i]);
        }

        if(!(departures[i] + delta <= arrivals[i]))
        {
            arrivals[i] = departures[i] + delta;
        }

        journeys.push_back({ 0, departures[i], arrivals[i]} );
    }

    return journeys;
}

auto random_not_only_incomparable_journeys(uint32_t amount, uint32_t tau,
                                           uint32_t delta) -> journey_vector
{
    std::uniform_int_distribution<uint32_t> dist(0, tau);
    journey_vector journeys;

    for(uint32_t i = 0; i < amount; ++i)
    {
        uint32_t departure = dist(generator());
        uint32_t arrival = dist(generator());

        if(!(departure < arrival))
        {
            std::swap(departure, arrival);
        }

        if(!(departure + delta <= arrival))
        {
            arrival = departure + delta;
        }

        journeys.push_back({ 0, departure, arrival });
    }

    return journeys;
}

auto make_mult_test_sets(const std::vector<tci::journey_schedule>& journeys)
    -> std::vector<mult_test_set>
{
    std::vector<mult_test_set> test_sets(static_cast<size_t>(std::log2(journeys.size()))
                                         - 1);

    for(size_t i = 0; i < journeys.size(); i += 2)
    {
        test_sets.back().emplace_back(journeys.begin() + static_cast<ptrdiff_t>(i),
                                      journeys.begin() + static_cast<ptrdiff_t>(i + 2));
    }

    for(size_t i = test_sets.size() - 1; i > 0; --i)
    {
        for(size_t j = 0; j < test_sets[i].size(); j += 2)
        {
            test_sets[i - 1].emplace_back(test_sets[i][j].begin(),
                                          test_sets[i][j].end());
            test_sets[i - 1].back().insert(test_sets[i - 1].back().end(),
                                           test_sets[i][j + 1].begin(),
                                           test_sets[i][j + 1].end());
        }
    }

    return test_sets;
}

void preprocess_journey_vectors_inside(std::vector<mult_test_set>* test_sets)
{
    for(auto& test: *test_sets)
    {
        for(auto& js: test)
        {
            sort_by_arrival_desc(&js);
            remove_by_discarding_rule_desc(&js);
        }
    }
}

auto test_single_test_set_sequential(const journey_vector& incomparables,
                                     const journey_vector& test) -> bool
{
    tci::detail::jtree t1;
    for(auto j: test)
    {
        t1.insert(j);
    }

    if(t1.size() != incomparables.size())
    {
        return false;
    }

    auto range =
        tci::range_pair(tci::range(t1.begin(), t1.end()),
                        tci::range(incomparables.begin(), incomparables.end()));

    return std::all_of(range.begin(), range.end(), [](auto elem) {
        return elem.first == elem.second;
    });
}

auto test_single_test_set_shuffled(const journey_vector& incomparables,
                                   journey_vector* test) -> bool
{
    shuffle_vector(test);
    return test_single_test_set_sequential(incomparables, *test);
}

auto test_mult_test_set_sequential(const journey_vector& incomparables,
                                   const mult_test_set& test) -> bool
{
    tci::detail::jtree t;
    for(const auto& js: test)
    {
        t.insert_by_decreasing_arrival(js.begin(), js.end());
    }

    if(t.size() != incomparables.size())
    {
        return false;
    }

    auto range =
        tci::range_pair(tci::range(t.begin(), t.end()),
                        tci::range(incomparables.begin(), incomparables.end()));

    return std::all_of(range.begin(), range.end(), [](auto elem) {
        return elem.first == elem.second;
    });
}

auto test_mult_test_set_shuffled(const journey_vector& incomparables,
                                 mult_test_set* test) -> bool
{
    shuffle_vector(test);
    return test_mult_test_set_sequential(incomparables, *test);
}

auto test_single_args_only_incomparable(uint32_t amount, uint32_t tau, uint32_t delta)
    -> bool
{
    auto journeys = random_only_incomparable_journeys(amount, tau, delta);
    auto incomparables = get_sorted_incomparables(journeys);

    auto res1 = test_single_test_set_sequential(incomparables, journeys);
    auto res2 = test_single_test_set_shuffled(incomparables, &journeys);

    return res1 && res2;
}

auto test_single_args_not_only_incomparable(uint32_t amount, uint32_t tau,
                                            uint32_t delta) -> bool
{
    auto journeys = random_not_only_incomparable_journeys(amount, tau, delta);
    auto incomparables = get_sorted_incomparables(journeys);

    auto res1 = test_single_test_set_sequential(incomparables, journeys);
    auto res2 = test_single_test_set_shuffled(incomparables, &journeys);

    return res1 && res2;
}

auto test_mult_args_only_incomparable(uint32_t amount, uint32_t tau, uint32_t delta)
    -> bool
{
    auto journeys = random_only_incomparable_journeys(amount, tau, delta);
    auto incomparables = get_sorted_incomparables(journeys);

    auto tests = make_mult_test_sets(journeys);

    return std::all_of(tests.begin(), tests.end(), [&](auto& test) {
        auto res1 = test_mult_test_set_sequential(incomparables, test);
        auto res2 = test_mult_test_set_shuffled(incomparables, &test);

        return res1 && res2;
    });
}

auto test_mult_args_not_only_incomparable(uint32_t amount, uint32_t tau, uint32_t delta)
    -> bool
{
    auto journeys = random_not_only_incomparable_journeys(amount, tau, delta);
    auto incomparables = get_sorted_incomparables(journeys);

    auto tests = make_mult_test_sets(journeys);
    preprocess_journey_vectors_inside(&tests);

    return std::all_of(tests.begin(), tests.end(), [&](auto& test) {
        auto res1 = test_mult_test_set_sequential(incomparables, test);
        auto res2 = test_mult_test_set_shuffled(incomparables, &test);

        return res1 && res2;
    });
}
