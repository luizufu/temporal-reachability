#include "tci/detail/contact-set.hxx"

#include <tuple>

namespace tci::detail
{

void contact_set::insert(contact c)
{
    _data.insert(c);
}

auto contact_set::contacts() const
    -> const std::set<contact, contact_set::timestamp_order>&
{
    return _data;
}

} // namespace tci::detail
