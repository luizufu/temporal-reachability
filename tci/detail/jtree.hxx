#pragma once

#include "tci/types.hxx"

#include <cereal/types/set.hpp>
#include <nostd/stl_set.hxx>
#include <optional>

namespace tci::detail
{
class jtree
{
    struct by_departure
    {
        auto operator()(label lhs, label rhs) const -> bool
        {
            return lhs.i.t1 < rhs.i.t1;
        }

        auto operator()(label lhs, timestamp t) const -> bool
        {
            return lhs.i.t1 < t;
        }

        auto operator()(timestamp t, label rhs) const -> bool
        {
            return t < rhs.i.t1;
        }
    };

    struct by_arrival
    {
        auto operator()(label lhs, label rhs) const -> bool
        {
            return lhs.i.t2 < rhs.i.t2;
        }

        auto operator()(label lhs, timestamp t) const -> bool
        {
            return lhs.i.t2 < t;
        }

        auto operator()(timestamp t, label lhs) const -> bool
        {
            return t < lhs.i.t2;
        }
    };

    nostd::set<label, by_arrival> _schedules;

public:
    using set_type = nostd::set<label, by_arrival>;
    using value_type = label;

    auto insert(label x) -> std::pair<set_type::const_iterator, bool>;

    [[nodiscard]] auto find_departure_geq(timestamp t) const
        -> std::optional<value_type>;

    [[nodiscard]] auto find_arrival_eq(timestamp t) const
        -> std::optional<value_type>;

    [[nodiscard]] auto find_arrival_leq(timestamp t) const
        -> std::optional<value_type>;

    [[nodiscard]] auto begin() const -> set_type::const_iterator;
    [[nodiscard]] auto end() const -> set_type::const_iterator;
    [[nodiscard]] auto rbegin() const -> set_type::const_reverse_iterator;
    [[nodiscard]] auto rend() const -> set_type::const_reverse_iterator;

    [[nodiscard]] auto size() const -> size_t;
    [[nodiscard]] auto empty() const -> bool;

    template<typename Archive>
    void serialize(Archive& ar);
};


template<typename Archive>
void jtree::serialize(Archive& ar)
{
    ar(_schedules);
}

} // namespace tci::detail
