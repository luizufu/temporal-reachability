#pragma once

#include <nostd/generator.hxx>
#include <cereal/types/vector.hpp>
#include <vector>
#include <functional>


namespace tci::detail
{
template<typename T>
class matrix
{
    std::vector<T> _data = {};
    size_t _n = 0;

public:
    matrix() = default;

    explicit matrix(size_t n)
        : _data(n * n)
        , _n(n)
    {
    }

    [[nodiscard]] auto get(size_t i, size_t j) const -> const T&
    {
        return _data.at(j * _n + i);
    }

    [[nodiscard]] auto get(size_t i, size_t j) -> T&
    {
        return _data[j * _n + i];
    }


    [[nodiscard]] auto rows(size_t i) const -> nostd::generator<std::pair<size_t, const T&>>
    {
        for(size_t j = 0; j < _n; ++j)
        {
            co_yield std::make_pair(j, get(i, j));
        }
    }

    [[nodiscard]] auto columns(size_t j) const -> nostd::generator<std::pair<size_t, const T&>>
    {
        for(size_t i = 0; i < _n; ++i)
        {
            co_yield std::make_pair(i, get(i, j));
        }
    }


    [[nodiscard]] auto n_rows() const -> size_t
    {
        return _n;
    }


    [[nodiscard]] auto n_columns() const -> size_t
    {
        return _n;
    }

    [[nodiscard]] auto size() const -> size_t
    {
        return _n * _n;
    }

    template<typename Archive>
    void serialize(Archive& ar)
    {
        ar(_data, _n);
    }
};

} // namespace tci::detail
