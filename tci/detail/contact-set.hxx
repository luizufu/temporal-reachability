#pragma once

#include "tci/types.hxx"

#include <cereal/types/set.hpp>
#include <set>

namespace tci::detail
{
class contact_set
{
    struct timestamp_order
    {
        auto operator()(const contact& lhs, const contact& rhs) const -> bool
        {
            auto tlhs = std::make_tuple(lhs.t, lhs.e.u, lhs.e.v);
            auto trhs = std::make_tuple(rhs.t, rhs.e.u, rhs.e.v);

            return tlhs < trhs;
        }
    };

    std::set<contact, timestamp_order> _data;

public:
    contact_set() = default;

    void insert(contact c);
    [[nodiscard]] auto contacts() const -> const std::set<contact, timestamp_order>&;

    template<typename Archive>
    void serialize(Archive& ar);
};

template<typename Archive>
void contact_set::serialize(Archive& ar)
{
    ar(_data);
}

} // namespace tci::detail
