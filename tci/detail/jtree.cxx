#include "tci/detail/jtree.hxx"

#include <algorithm>

namespace tci::detail
{

auto jtree::insert(label x) -> std::pair<set_type::const_iterator, bool>
{
    if(_schedules.empty())
    {
        return std::make_pair(_schedules.insert(_schedules.end(), x), true);
    }

    auto it = _schedules.upper_bound(x.i.t2, by_arrival());

    if(it == _schedules.begin() || !includes(x.i, std::prev(it)->i))
    {
        if(it != _schedules.begin() && std::prev(it)->i.t2 == x.i.t2)
        {
            --it;
        }

        while(it != _schedules.end() && includes(it->i, x.i))
        {
            it = _schedules.erase(it);
        }

        auto res = _schedules.insert(it, x, by_arrival());
        return std::make_pair(res, true);
    }

    return std::make_pair(it, false);
}


auto jtree::find_departure_geq(timestamp t) const -> std::optional<value_type>
{
    auto it = _schedules.lower_bound(t, by_departure());
    return (it != _schedules.end())
        ? std::optional(*it)
        : std::nullopt;
}

auto jtree::find_arrival_eq(timestamp t) const -> std::optional<value_type>
{
    auto it = _schedules.lower_bound(t, by_arrival());
    return (it != _schedules.end() && it->i.t1 == t)
        ? std::optional(*it)
        : std::nullopt;
}

auto jtree::find_arrival_leq(timestamp t) const
    -> std::optional<value_type>
{
    auto it = set_type::const_reverse_iterator(
            _schedules.upper_bound(t, by_arrival()));
    return it != _schedules.rend()
        ? std::optional(*it)
        : std::nullopt;
}

auto jtree::begin() const -> set_type::const_iterator
{
    return _schedules.begin();
}

auto jtree::end() const -> set_type::const_iterator
{
    return _schedules.end();
}

auto jtree::rbegin() const -> set_type::const_reverse_iterator
{
    return _schedules.rbegin();
}

auto jtree::rend() const -> set_type::const_reverse_iterator
{
    return _schedules.rend();
}

auto jtree::size() const -> size_t
{
    return _schedules.size();
}

auto jtree::empty() const -> bool
{
    return _schedules.empty();
}

} // namespace tci::detail
