#pragma once

#include "tci/index.hxx"
#include "tci/detail/contact-set.hxx"

#include <cereal/types/polymorphic.hpp>
#include <unordered_map>

namespace tci
{
class baseline : public index
{
    detail::contact_set _data;

public:
    baseline() = default;
    baseline(size_t n, timestamp tau, timestamp delta) : index(n, tau, delta) {}

    void add_contact(vertex u, vertex v, timestamp t) override;

    [[nodiscard]] auto can_reach(vertex u, vertex v, interval i) const
        -> bool override;

    [[nodiscard]] auto reconstruct_journey(vertex u, vertex v,
                                        interval i) const
        -> std::optional<journey> override;

    template<typename Archive>
    void serialize(Archive& ar);
};

template<typename Archive>
void baseline::serialize(Archive& ar)
{
    ar(cereal::base_class<index>(this), _data);
}

} // namespace tci

#include <cereal/archives/binary.hpp>
CEREAL_REGISTER_TYPE(tci::baseline) // NOLINT
