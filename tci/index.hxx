#pragma once

#include "tci/types.hxx"

#include <cereal/types/base_class.hpp>
#include <optional>

namespace tci
{
class index
{
    size_t _n = 0;
    timestamp _tau = 0;
    timestamp _delta = 0;

protected:
    index() = default;
    index(size_t n, timestamp tau, timestamp delta) : _n(n), _tau(tau), _delta(delta) {}

public:
    virtual ~index() = default;

    virtual void add_contact(vertex u, vertex v, timestamp t) = 0;

    [[nodiscard]] virtual auto can_reach(vertex u, vertex v,
                                                interval i) const -> bool = 0;

    [[nodiscard]] virtual auto is_connected(interval i) const -> bool
    {
        for(size_t u = 0; u < _n; ++u)
            for(size_t v = 0; v < _n; ++v)
                if(!can_reach(u, v, i))
                    return false;

        return true;
    }

    [[nodiscard]] virtual auto reconstruct_journey(vertex u, vertex v,
                                                interval i) const
        -> std::optional<journey> = 0;

    [[nodiscard]] auto n() const -> timestamp  { return _n; }
    [[nodiscard]] auto tau() const -> timestamp  { return _tau; }
    [[nodiscard]] auto delta() const -> timestamp { return _delta; }

    template<typename Archive>
    void serialize(Archive& ar);
};

template<typename Archive>
void index::serialize(Archive& ar)
{
    ar(_n, _tau, _delta);
}

} // namespace tci
