#include "tci/types.hxx"

#include "cereal/types/vector.hpp"

#include <cstdint>
#include <iostream>
#include <vector>
#include <tuple>

namespace tci
{

// edge functions

auto operator==(const edge& lhs, const edge& rhs) -> bool
{
    return std::make_tuple(lhs.u, lhs.v) == std::make_tuple(rhs.u, rhs.v);
}

auto operator!=(const edge& lhs, const edge& rhs) -> bool
{
    return !(lhs == rhs);
}

auto operator>>(std::istream& in, edge& e) -> std::istream&
{
    in >> e.u >> e.v;
    return in;
}

auto operator<<(std::ostream& out, const edge& e) -> std::ostream&
{
    out << '(' << e.u << ", " << e.v << ')';
    return out;
}

// contact functions

auto operator==(const contact& lhs, const contact& rhs) -> bool
{
    return std::make_tuple(lhs.e, lhs.t) == std::make_tuple(rhs.e, rhs.t);
}

auto operator>>(std::istream& in, contact& c) -> std::istream&
{
    in >> c.e.u >> c.e.v >> c.t;
    return in;
}

auto operator<<(std::ostream& out, const contact& c) -> std::ostream&
{
    out << '(' << c.e << ", " << c.t << ')';
    return out;
}


// interval

auto includes(interval lhs, interval rhs) -> bool
{
    return rhs.t1 <= lhs.t1 && lhs.t2 <= rhs.t2;
}

auto is_left_of(interval lhs, interval rhs) -> bool
{
    return lhs.t2 <= rhs.t1;
}

auto operator==(const interval& lhs, const interval& rhs) -> bool
{
    return std::make_tuple(lhs.t1, lhs.t2)
        == std::make_tuple(rhs.t1, rhs.t2);
}

auto operator>>(std::istream& in, interval& i) -> std::istream&
{
    in >> i.t1 >> i.t2;
    return in;
}

auto operator<<(std::ostream& out, const interval& i) -> std::ostream&
{
    out << '[' << i.t1 << ", " << i.t2 << ']';
    return out;
}


// label

auto concatenate(label lhs, const label& rhs) -> label
{
    lhs.i.t2 = rhs.i.t2;
    return lhs;
}

auto operator==(const label& lhs, const label& rhs) -> bool
{
    return std::make_tuple(lhs.hop, lhs.i)
        == std::make_tuple(rhs.hop, rhs.i);
}

auto operator!=(const label& lhs, const label& rhs) -> bool
{
    return !(lhs == rhs);
}

auto operator<<(std::ostream& out, const label& l) -> std::ostream&
{
    out << '[' << l.hop << ", " << l.i << ']';
    return out;
}


// labeled_edge functions

auto concatenate(labeled_edge lhs, const labeled_edge& rhs) -> labeled_edge
{
    lhs.l = concatenate(lhs.l, rhs.l);
    lhs.v = rhs.v;
    return lhs;
}

auto operator==(const labeled_edge& lhs, const labeled_edge& rhs) -> bool
{
    return std::make_tuple(lhs.u, lhs.v, lhs.l)
        == std::make_tuple(rhs.u, rhs.v, rhs.l);
}

auto operator!=(const labeled_edge& lhs, const labeled_edge& rhs) -> bool
{
    return !(lhs == rhs);
}

auto operator<<(std::ostream& out, const labeled_edge& r) -> std::ostream&
{
    out << '[' << r.u << ", " << r.v << ", " << r.l << ']';
    return out;
}



// journey functions

auto concatenate(journey lhs, const journey& rhs) -> journey
{
    lhs._contacts.insert(lhs._contacts.end(), rhs._contacts.begin(),
                         rhs._contacts.end());

    return lhs;
}

auto operator<<(std::ostream& out, const journey& j) -> std::ostream&
{
    if(j._contacts.empty())
    {
        return out;
    }

    out << j._contacts[0];

    for(size_t i = 1; i < j._contacts.size(); ++i)
    {
        out << ", " << j._contacts[i];
    }

    return out;
}

auto operator==(const journey& lhs, const journey& rhs) -> bool
{
    return lhs.departure() == rhs.departure() && lhs.arrival() == rhs.arrival();
}

auto operator!=(const journey& lhs, const journey& rhs) -> bool
{
    return !(lhs == rhs);
}


} // namespace tci

auto std::hash<tci::edge>::operator()(const tci::edge& e) const -> std::size_t
{
    static auto h = std::hash<uint32_t>();
    return (h(e.u) ^ (h(e.v) << 1UL));
}

auto std::hash<tci::contact>::operator()(const tci::contact& c) const -> std::size_t
{
    static auto h = std::hash<uint32_t>();
    return (h(c.e.u) ^ (h(c.e.v) << 1UL)) ^ (h(c.t) << 1UL);
}
