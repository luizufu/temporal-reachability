#pragma once

#include "tci/index.hxx"
#include "tci/detail/jtree.hxx"
#include "tci/detail/matrix.hxx"

#include <cereal/types/polymorphic.hpp>

namespace tci
{
class transitive_closure : public index
{
    detail::matrix<detail::jtree> _matrix;

public:
    transitive_closure() = default;
    transitive_closure(uint32_t n, timestamp tau, timestamp delta) : index(n, tau, delta), _matrix(n) {}

    void add_contact(vertex u, vertex v, vertex t) override;

    [[nodiscard]] auto can_reach(vertex u, vertex v, interval i) const
        -> bool override;

    [[nodiscard]] auto reconstruct_journey(vertex u, vertex v,
                                        interval i) const
        -> std::optional<journey> override;

    template<typename Archive>
    void serialize(Archive& ar);
};

template<typename Archive>
void transitive_closure::serialize(Archive& ar)
{
    ar(cereal::base_class<index>(this), _matrix);
}

} // namespace tci

#include <cereal/archives/binary.hpp>
CEREAL_REGISTER_TYPE(tci::transitive_closure) // NOLINT
