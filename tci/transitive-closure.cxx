#include "tci/transitive-closure.hxx"

#include <algorithm>
#include <queue>

namespace tci
{

template <typename Matrix>
static auto materialize(vertex from, vertex to, label l, timestamp delta, const Matrix& m) -> journey
{
    journey j(delta);

    while(l.hop != to)
    {
        j = concatenate(j, journey({{from, l.hop}, l.i.t1}, delta));

        if(const auto& tree = m.get(l.hop, to); !tree.empty())
        {
            if(auto i = tree.find_arrival_eq(l.i.t2))
            {
                from = l.hop;
                l = *i;
            }
        }
    }

    j = concatenate(j, journey({{from, l.hop}, l.i.t1}, delta));
    return j;
}


void transitive_closure::add_contact(vertex u, vertex v, timestamp t)
{
    if(u == v || t >= tau())
    {
        return;
    }

    if(_matrix.get(u, v).insert({ v, { t, t + delta() }}).second)
    {
        std::vector<std::pair<vertex, label>> ls;

        for(auto& [w, tree]: _matrix.rows(u))
        {
            if(w == v)
            {
                continue;
            }

            if(auto tmp = tree.find_arrival_leq(t))
            {
                label l = { tmp->hop, { tmp->i.t1, t } };
                if(_matrix.get(w, v).insert(l).second)
                {
                    ls.emplace_back(w, l);
                }
            }
        }

        for(auto& [ww, tree] : _matrix.columns(v))
        {
            if(ww == u)
            {
                continue;
            }

            if(auto tmp = tree.find_departure_geq(t + delta()))
            {
                label ll = { v, { t + delta(), tmp->i.t2 } };
                if(_matrix.get(u, ww).insert(ll).second)
                {
                    for(const auto& [w, l]: ls)
                    {
                        label lll = { l.hop, { l.i.t1, ll.i.t2 } };
                        _matrix.get(w, ww).insert(lll);
                    }
                }
            }
        }
    }
}


auto transitive_closure::can_reach(vertex u, vertex v, interval i) const
    -> bool
{
    if(u == v)
    {
        return true;
    }

    i.t2 = std::min(i.t2, tau());

    if(i.t1 >= tau() || i.t2 - i.t1 < delta())
    {
        return false;
    }

    if(const auto& tree = _matrix.get(u, v); !tree.empty())
    {
        if(i.t1 == 0 && i.t2 == tau())
        {
            return true;
        }

        if(auto tmp = tree.find_departure_geq(i.t1))
        {
            return tmp->i.t2 < i.t2;
        }
    }

    return false;
}


auto transitive_closure::reconstruct_journey(vertex u, vertex v,
                                          interval i) const
    -> std::optional<journey>
{
    if(u == v)
    {
        return journey(delta());;
    }

    i.t2 = std::min(i.t2, tau());

    if(i.t1 >= tau() || i.t2 - i.t1 < delta())
    {
        return {};
    }

    if(const auto& tree = _matrix.get(u, v); !tree.empty())
    {
        if(auto tmp = tree.find_departure_geq(i.t1); tmp->i.t2 < i.t2)
        {
            return materialize(u, v, *tmp, delta(), _matrix);
        }
    }

    return {};
}


} // namespace tci
