#pragma once

#include "cereal/types/vector.hpp"

#include <cereal/types/polymorphic.hpp>
#include <cstdint>
#include <iostream>

namespace tci
{
using vertex = uint32_t;
using timestamp = uint32_t;

struct edge
{
    vertex u, v;

    template<typename Archive>
    void serialize(Archive& ar)
    {
        ar(u, v);
    }
};

auto operator==(const edge& lhs, const edge& rhs) -> bool;
auto operator!=(const edge& lhs, const edge& rhs) -> bool;
auto operator>>(std::istream& in, edge& e) -> std::istream&;
auto operator<<(std::ostream& out, const edge& e) -> std::ostream&;


struct contact
{
    edge e;
    timestamp t;

    template<typename Archive>
    void serialize(Archive& ar)
    {
        ar(e, t);
    }
};

auto operator==(const contact& lhs, const contact& rhs) -> bool;
auto operator!=(const contact& lhs, const contact& rhs) -> bool;
auto operator>>(std::istream& in, contact& c) -> std::istream&;
auto operator<<(std::ostream& out, const contact& c) -> std::ostream&;


struct interval
{
    timestamp t1;
    timestamp t2;

    template<typename Archive>
    void serialize(Archive& ar)
    {
        ar(t1, t2);
    }
};

auto includes(interval lhs, interval rhs) -> bool;
auto is_left_of(interval lhs, interval rhs) -> bool;
auto operator==(const interval& lhs, const interval& rhs) -> bool;
auto operator>>(std::istream& in, interval& i) -> std::istream&;
auto operator<<(std::ostream& out, const interval& i) -> std::ostream&;


struct label
{
    vertex hop;
    interval i;

    template<typename Archive>
    void serialize(Archive& ar)
    {
        ar(hop, i);
    }
};

auto concatenate(label lhs, const label& rhs) -> label;
auto is_subset_of(label lhs, const label& rhs) -> bool;
auto is_left_of(label lhs, const label& rhs) -> bool;
auto operator==(const label& lhs, const label& rhs) -> bool;
auto operator!=(const label& lhs, const label& rhs) -> bool;
auto operator<<(std::ostream& out, const label& i) -> std::ostream&;


struct labeled_edge
{
    vertex u, v;
    label l;

    template<typename Archive>
    void serialize(Archive& ar)
    {
        ar(u, v, l);
    }
};

auto concatenate(labeled_edge lhs, const labeled_edge& rhs) -> labeled_edge;
auto operator==(const labeled_edge& lhs, const labeled_edge& rhs) -> bool;
auto operator!=(const labeled_edge& lhs, const labeled_edge& rhs) -> bool;
auto operator<<(std::ostream& out, const labeled_edge& r) -> std::ostream&;



class journey
{
    friend class transitive_closure;

    std::vector<contact> _contacts;
    timestamp _delta;

public:
    explicit journey(timestamp delta)
        : _delta(delta)
    {
    }

    journey(contact c, timestamp delta)
        : _contacts {c}
        , _delta(delta)
    {
    }

    [[nodiscard]] auto from() const -> vertex
    {
        return _contacts.front().e.u;
    }

    [[nodiscard]] auto to() const -> vertex
    {
        return _contacts.back().e.v;
    }

    [[nodiscard]] auto departure() const -> timestamp
    {
        return _contacts.front().t;
    }

    [[nodiscard]] auto arrival() const -> timestamp
    {
        return _contacts.back().t + _delta;
    }

    [[nodiscard]] auto duration() const -> timestamp
    {
        return arrival() - departure();
    }

    [[nodiscard]] auto size() const -> size_t
    {
        return _contacts.size();
    }

    template<typename Archive>
    void serialize(Archive& ar)
    {
        ar(_contacts, _delta);
    }

    friend auto concatenate(journey lhs, const journey& rhs) -> journey;
    friend auto operator<<(std::ostream& out, const journey& j) -> std::ostream&;
};

auto concatenate(journey lhs, const journey& rhs) -> journey;
auto operator==(const journey& lhs, const journey& rhs) -> bool;
auto operator!=(const journey& lhs, const journey& rhs) -> bool;
auto operator<<(std::ostream& out, const journey& j) -> std::ostream&;


} // namespace tci

namespace std
{
template<>
struct hash<tci::edge>
{
    auto operator()(const tci::edge& e) const -> std::size_t;
};

template<>
struct hash<tci::contact>
{
    auto operator()(const tci::contact& c) const -> std::size_t;
};
} // namespace std
