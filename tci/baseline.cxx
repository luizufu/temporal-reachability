#include "tci/baseline.hxx"

#include <algorithm>
#include <queue>

namespace tci
{
using journey_map = std::unordered_map<vertex, std::vector<journey>>;

template<typename StopFunc>
static auto construct_trivial(const detail::contact_set& contacts,
        vertex source, interval i, timestamp delta,
                              StopFunc stop_fn) -> journey_map
{
    journey_map map;

    map[source] = {journey({{source, source}, 0}, delta)};
    contact last_contact = {{source, source}, 0};

    for(const contact& c: contacts.contacts())
    {
        if(c.t < i.t1 || (c == last_contact))
        {
            continue;
        }

        if(c.t + delta >= i.t2)
        {
            break;
        }

        if(auto it = map.find(c.e.u); it != map.end() &&  std::any_of(
                    it->second.begin(), it->second.end(), [source, t=c.t](const journey& j) {
                        return (j.from() == source && j.to() == source) || (j.arrival() <= t);
                    }))
        {
            map[c.e.v].emplace_back(c, delta);

            if(stop_fn(c))
            {
                break;
            }
        }

        last_contact = c;
    }

    return map;
}

void baseline::add_contact(vertex u, vertex v, timestamp t)
{
    if(u == v || t >= tau())
    {
        return;
    }

    _data.insert({{ u, v }, t });
}

auto baseline::can_reach(vertex u, vertex v, interval i) const -> bool
{
    if(u == v)
    {
        return true;
    }

    i.t2 = std::min(i.t2, tau());

    if(i.t1 >= tau() || i.t2 - i.t1 < delta())
    {
        return false;
    }

    auto trivial_journeys =
        construct_trivial(_data, u, i, delta(), [v](const contact& c) {
            return c.e.v == v;
        });

    return trivial_journeys.count(v) > 0;
}


auto baseline::reconstruct_journey(vertex u, vertex v, interval i) const
    -> std::optional<journey>
{

    if(u == v)
    {
        return journey(delta());
    }

    i.t2 = std::min(i.t2, tau());

    if(i.t1 >= tau() || i.t2 - i.t1 < delta())
    {
        return {};
    }

    auto trivial_journeys =
        construct_trivial(_data, u, i, delta(), [v](const contact& c) {
            return c.e.v == v;
        });

    if(trivial_journeys.count(v) == 0)
    {
        return {};
    }

    std::vector<journey> js;
    std::queue<journey> q;

    for(const journey& j: trivial_journeys[v])
    {
        q.push(j);
    }

    while(!q.empty())
    {
        journey j = q.front();
        q.pop();

        for(const journey& candidate: trivial_journeys[j.from()])
        {
            if(candidate.from() == u && candidate.to() == u)
            {
                if(j.duration() < tau() / delta())
                {
                    js.push_back(j);
                }
            }
            else
            {
                q.push(concatenate(candidate, j));
            }
        }
    }

    return js[0];
}


} // namespace tci
