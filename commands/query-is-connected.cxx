#include "commands/query-is-connected.hxx"

query_is_connected::query_is_connected(const std::unique_ptr<tci::index>& index)
    : const_command(index)
{
}

auto query_is_connected::process(const std::vector<std::string>& args) const
    -> exit_code
{
    tci::interval i = {static_cast<tci::timestamp>(std::stoul(args[1])),
                                    static_cast<tci::timestamp>(std::stoul(args[2]))};

    std::cout << (index()->is_connected(i) ? "CONNECTED" : "DISCONNECTED")
              << std::endl;

    return exit_code::success;
}
