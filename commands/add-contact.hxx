#pragma once

#include "commands/command.hxx"

struct add_contact : const_command<add_contact>
{
    static constexpr uint32_t number_of_args = 4;
    static constexpr command_help help = {"Add a new contact (u, v, t)",
                                          "[u] [v] [t]"};

    explicit add_contact(const std::unique_ptr<tci::index>& index);
    [[nodiscard]] auto process(const std::vector<std::string>& args) const -> exit_code;
};
