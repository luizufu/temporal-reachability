#include "commands/load-index.hxx"

#include <cereal/archives/binary.hpp>
#include <fstream>

load_index::load_index(std::unique_ptr<tci::index>* index)
    : command(index)
{
}

auto load_index::process(const std::vector<std::string>& args) -> exit_code
{
    if(auto ifs = std::ifstream(args[1], std::ios::binary))
    {
        cereal::BinaryInputArchive archive(ifs);
        archive >> *index();
        return exit_code::success;
    }

    return exit_code::error;
}
