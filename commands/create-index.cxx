#include "commands/create-index.hxx"

#include "tci/baseline.hxx"
#include "tci/transitive-closure.hxx"

create_index::create_index(std::unique_ptr<tci::index>* index)
    : command(index)
{
}

auto create_index::process(const std::vector<std::string>& args) -> exit_code
{
    std::string type = args[1];
    uint32_t n = std::stoul(args[2]);
    tci::timestamp tau = std::stoul(args[3]);
    tci::timestamp delta = std::stoul(args[4]);

    if(type == "baseline")
    {
        index() = std::make_unique<tci::baseline>(n, tau, delta);
    }
    else if(type == "tc")
    {
        index() = std::make_unique<tci::transitive_closure>(n, tau, delta);
    }
    else
    {
        return exit_code::invalid_arguments;
    }

    return exit_code::success;
}
