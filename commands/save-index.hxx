#pragma once

#include "commands/command.hxx"

struct save_index : const_command<save_index>
{
    static constexpr uint32_t number_of_args = 2;
    static constexpr command_help help = {"Save the current index", "[file]"};

    explicit save_index(const std::unique_ptr<tci::index>& index);
    [[nodiscard]] auto process(const std::vector<std::string>& args) const -> exit_code;
};
