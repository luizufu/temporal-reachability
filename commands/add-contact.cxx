#include "commands/add-contact.hxx"

add_contact::add_contact(const std::unique_ptr<tci::index>& index)
    : const_command(index)
{
}

auto add_contact::process(const std::vector<std::string>& args) const -> exit_code
{
    tci::vertex u = std::stoul(args[1]);
    tci::vertex v = std::stoul(args[2]);
    tci::timestamp t = std::stoul(args[3]);

    index()->add_contact(u, v, t);

    return exit_code::success;
}
