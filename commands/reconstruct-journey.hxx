#pragma once

#include "commands/command.hxx"

struct reconstruct_journey : const_command<reconstruct_journey>
{
    static constexpr uint32_t number_of_args = 5;
    static constexpr command_help help = {"Reconstrut a journey u ~~> v in [t1, t2]",
                                          "[u] [v] [t1] [t2]"};

    explicit reconstruct_journey(const std::unique_ptr<tci::index>& index);
    [[nodiscard]] auto process(const std::vector<std::string>& args) const -> exit_code;
};
