#include "commands/reconstruct-journey.hxx"

reconstruct_journey::reconstruct_journey(const std::unique_ptr<tci::index>& index)
    : const_command(index)
{
}

auto reconstruct_journey::process(const std::vector<std::string>& args) const
    -> exit_code
{
    auto u = static_cast<tci::vertex>(std::stoul(args[1]));
    auto v = static_cast<tci::vertex>(std::stoul(args[2]));

    tci::interval i = {static_cast<tci::timestamp>(std::stoul(args[3])),
                                    static_cast<tci::timestamp>(std::stoul(args[4]))};

    if(auto j = index()->reconstruct_journey(u, v, i))
    {
        std::cout << *j << '\n';
    }

    return exit_code::success;
}
