#pragma once

#include "commands/command.hxx"

struct query_is_connected : const_command<query_is_connected>
{
    static constexpr uint32_t number_of_args = 3;
    static constexpr command_help help = {"Check if every u ~~> v in [t1, t2]",
                                          "[t1] [t2]"};

    explicit query_is_connected(const std::unique_ptr<tci::index>& index);
    [[nodiscard]] auto process(const std::vector<std::string>& args) const -> exit_code;
};
