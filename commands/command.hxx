#pragma once

#include "prompt.hxx"
#include "tci/index.hxx"

#include <memory>
#include <string>
#include <vector>

template<typename Child>
class command
{
    std::unique_ptr<tci::index>* _index;

    [[nodiscard]] auto child() -> Child&
    {
        return *static_cast<Child*>(this);
    }

protected:
    auto index() -> std::unique_ptr<tci::index>&
    {
        return *_index;
    }

public:
    explicit command(std::unique_ptr<tci::index>* index)
        : _index(index)
    {
    }

    auto operator()(const std::vector<std::string>& args) -> exit_code
    {
        if(_index == nullptr)
        {
            return exit_code::error;
        }

        if(args.size() < child().number_of_args)
        {
            return exit_code::invalid_arguments;
        }

        return child().process(args);
    }
};

template<typename Child>
class const_command
{
    const std::unique_ptr<tci::index>& _index;

    [[nodiscard]] auto child() const -> const Child&
    {
        return *static_cast<const Child*>(this);
    }

protected:
    [[nodiscard]] auto index() const -> const std::unique_ptr<tci::index>&
    {
        return _index;
    }

public:
    explicit const_command(const std::unique_ptr<tci::index>& index)
        : _index(index)
    {
    }

    auto operator()(const std::vector<std::string>& args) const -> exit_code
    {
        if(args.size() < child().number_of_args)
        {
            return exit_code::invalid_arguments;
        }

        return child().process(args);
    }
};
