#include "commands/save-index.hxx"

#include <cereal/archives/binary.hpp>
#include <fstream>

save_index::save_index(const std::unique_ptr<tci::index>& index)
    : const_command(index)
{
}

auto save_index::process(const std::vector<std::string>& args) const -> exit_code
{
    if(auto ofs = std::ofstream(args[1], std::ios::binary))
    {
        cereal::BinaryOutputArchive archive(ofs);
        archive << *index();
        return exit_code::success;
    }

    return exit_code::error;
}
