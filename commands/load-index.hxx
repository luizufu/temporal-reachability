#pragma once

#include "commands/command.hxx"

struct load_index : command<load_index>
{
    static constexpr uint32_t number_of_args = 2;
    static constexpr command_help help = {"load an index", "[file]"};

    explicit load_index(std::unique_ptr<tci::index>* index);
    auto process(const std::vector<std::string>& args) -> exit_code;
};
