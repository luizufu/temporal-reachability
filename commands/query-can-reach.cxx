#include "commands/query-can-reach.hxx"

query_can_reach::query_can_reach(const std::unique_ptr<tci::index>& index)
    : const_command(index)
{
}

auto query_can_reach::process(const std::vector<std::string>& args) const
    -> exit_code
{
    auto u = static_cast<tci::vertex>(std::stoul(args[1]));
    auto v = static_cast<tci::vertex>(std::stoul(args[2]));

    tci::interval i = {static_cast<tci::timestamp>(std::stoul(args[3])),
                                    static_cast<tci::timestamp>(std::stoul(args[4]))};

    std::cout << (index()->can_reach(u, v, i) ? "CAN REACH" : "CANNOT REACH")
              << std::endl;

    return exit_code::success;
}
