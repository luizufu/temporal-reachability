#pragma once

#include "commands/command.hxx"

struct create_index : command<create_index>
{
    static constexpr uint32_t number_of_args = 5;
    static constexpr command_help help = {"Create an index of type=baseline/tc",
                                          "[type] [n] [tau] [delta]"};

    explicit create_index(std::unique_ptr<tci::index>* index);
    auto process(const std::vector<std::string>& args) -> exit_code;
};
