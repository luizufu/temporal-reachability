#pragma once

#include "commands/command.hxx"

struct query_can_reach : const_command<query_can_reach>
{
    static constexpr uint32_t number_of_args = 5;
    static constexpr command_help help = {"Check if there is u ~~> v in [t1, t2]",
                                          "[u] [v] [t1] [t2]"};

    explicit query_can_reach(const std::unique_ptr<tci::index>& index);
    [[nodiscard]] auto process(const std::vector<std::string>& args) const -> exit_code;
};
