#include "commands/create-index.hxx"
#include "commands/load-index.hxx"
#include "commands/save-index.hxx"
#include "commands/add-contact.hxx"
#include "commands/query-can-reach.hxx"
#include "commands/query-is-connected.hxx"
#include "commands/reconstruct-journey.hxx"
#include "prompt.hxx"


auto main() -> int
{
    std::string greeting = "Bien venu mon chèr ami!\n"
                           "Tappes \"help\" si tu a besoin d'aide\n"
                           "Tappes \"quit\" pour quitter\n\n"
                           "S'amuser!";

    std::string farewell = "Au revoir!\n";

    prompt cli("$>", greeting, farewell);

    std::unique_ptr<tci::index> ctx;

    cli.add_command("create", create_index(&ctx), create_index::help);
    cli.add_command("load", load_index(&ctx), load_index::help);
    cli.add_command("save", save_index(ctx), save_index::help);
    cli.add_line();

    cli.add_command("add_contact", add_contact(ctx), add_contact::help);
    cli.add_line();

    cli.add_command("can_reach", query_can_reach(ctx), query_can_reach::help);
    cli.add_command("is_connected", query_is_connected(ctx), query_is_connected::help);
    cli.add_line();

    cli.add_command("journey", reconstruct_journey(ctx), reconstruct_journey::help);
    cli.add_line();


    cli.start();
    return 0;
}
