#pragma once

#include <functional>
#include <memory>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

struct command_help
{
    std::string_view description; // NOLINT
    std::string_view arguments;   // NOLINT

    constexpr command_help(std::string_view desc, std::string_view args)
        : description(desc)
        , arguments(args)
    {
    }
};

enum class exit_code : int
{
    success = 0,
    invalid_arguments = 1,
    error = 9
};

class prompt
{
    using cmd_callable = std::function<exit_code(const std::vector<std::string>&)>;

    struct command
    {
        std::string name;
        cmd_callable cmd;
        command_help help;
    };

    std::string _delimiter;
    std::string _greeting;
    std::string _farewell;

    size_t _col1_width;
    size_t _col2_width;

    std::unordered_map<std::string, size_t> _mapping;
    std::vector<command> _commands;
    std::vector<bool> _lines;

    void print_help();

public:
    prompt(std::string delim, std::string greeting, std::string farewell);

    void add_command(std::string name, cmd_callable cmd, command_help help);
    void add_line();
    void start();
};
