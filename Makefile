CXX=g++
PROFILE=-g
SANITIZER=
CXXFLAGS=-std=c++2a -fcoroutines -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wmissing-declarations -Wmissing-include-dirs -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-overflow=5 -Wundef -Werror -Wno-unused -Wno-unknown-argument -Wno-unknown-warning-option
LDFLAGS=
BUILD_DIR=build
HEADERS=-I . -isystem deps

SOURCES := $(patsubst ./%,%, $(shell find . -name "*.cxx"))
DEPENDS := $(patsubst %.cxx,$(BUILD_DIR)/%.d, $(SOURCES))
SOURCES := $(filter-out main.cxx, $(SOURCES))
SOURCES := $(filter-out tests/basic.cxx, $(SOURCES))
SOURCES := $(filter-out tests/jtree.cxx, $(SOURCES))
SOURCES := $(filter-out tests/transitive-closure.cxx, $(SOURCES))
SOURCES := $(filter-out tests/journeys.cxx, $(SOURCES))
OBJECTS := $(patsubst %.cxx,$(BUILD_DIR)/%.o, $(SOURCES))

all: $(BUILD_DIR)/main

everything: all tests

$(BUILD_DIR)/main: $(OBJECTS) $(BUILD_DIR)/main.o
	@mkdir -p $(@D)
	$(CXX) $(PROFILE) $(SANITIZER) $(CXXFLAGS) $^ -o $@ $(LDFLAGS)

$(BUILD_DIR)/tests/jtree: $(OBJECTS) $(BUILD_DIR)/tests/jtree.o
	@mkdir -p $(@D)
	$(CXX) $(PROFILE) $(SANITIZER) $(CXXFLAGS) $^ -o $@ $(LDFLAGS)

$(BUILD_DIR)/tests/basic: $(OBJECTS) $(BUILD_DIR)/tests/basic.o
	@mkdir -p $(@D)
	$(CXX) $(PROFILE) $(SANITIZER) $(CXXFLAGS) $^ -o $@ $(LDFLAGS)

$(BUILD_DIR)/tests/transitive-closure: $(OBJECTS) $(BUILD_DIR)/tests/transitive-closure.o
	@mkdir -p $(@D)
	$(CXX) $(PROFILE) $(SANITIZER) $(CXXFLAGS) $^ -o $@ $(LDFLAGS)

$(BUILD_DIR)/tests/journeys: $(OBJECTS) $(BUILD_DIR)/tests/journeys.o
	@mkdir -p $(@D)
	$(CXX) $(PROFILE) $(SANITIZER) $(CXXFLAGS) $^ -o $@ $(LDFLAGS)


-include $(DEPENDS)

$(BUILD_DIR)/%.o: %.cxx Makefile
	@mkdir -p $(@D)
	$(CXX) $(PROFILE) $(SANITIZER) $(CXXFLAGS) $(HEADERS) -MMD -MP -c $< -o $@


.PHONY: tests run-tests database cppcheck tidy format clean

# tests: $(BUILD_DIR)/tests/jtree $(BUILD_DIR)/tests/indexed-jtree $(BUILD_DIR)/tests/transitive-closure $(BUILD_DIR)/tests/transitive-closure2 $(BUILD_DIR)/tests/journeys
tests: $(BUILD_DIR)/tests/basic $(BUILD_DIR)/tests/jtree $(BUILD_DIR)/tests/transitive-closure $(BUILD_DIR)/tests/journeys


run-tests: tests
	# tests/test-commands.exp $(BUILD_DIR)/main idx531359.bin
	# @rm idx531359.bin
	$(BUILD_DIR)/tests/jtree
	$(BUILD_DIR)/tests/transitive-closure
	$(BUILD_DIR)/tests/journeys

database: clean
	bear make
	sed -i '/-fcoroutines/d' compile_commands.json

cppcheck: compile_commands.json
	cppcheck --enable=all --project=compile_commands.json 2> $(BUILD_DIR)/cppcheck.out

tidy: /usr/share/clang/run-clang-tidy.py .clang-tidy compile_commands.json
	/usr/share/clang/run-clang-tidy.py > $(BUILD_DIR)/clang-tidy.out

format: .clang-format
	find . -regex '.*\.\(cxx\|hxx\)' -exec clang-format -style=file -i {} \;

clean:
	rm -rf $(BUILD_DIR)
